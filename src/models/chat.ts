import {Message} from "./message";

export interface Chat {
    id: number,
    user1: number,
    user2: number
}


export type NewChat = Omit<Chat, "id">
