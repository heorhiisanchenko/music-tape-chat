import dotenv from 'dotenv';
import path from 'path'

const envPath = path.join(__dirname, '../dev.env')
dotenv.config({path: envPath});
import server from "./server";
import {DBConnection} from "./repos/db-connection";

const PORT = process.env.APP_PORT || 9457;
console.log({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: parseInt(process.env.DB_PORT || '')
})

const init = async () => {
    const con = await DBConnection.getInstance()
    con.init();
    await con.dropTables();
    await con.createTables();
    // await con.insertSampleData()
    server.listen(PORT, () => {
        console.log('Express server started on port: ' + PORT)
    });
}

init()

